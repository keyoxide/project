---
name: 'Community Call Agenda'
about: 'Prepare the agenda for the next Community Call'
title: 'Community Call #'
labels:
  - 'Scope/Community Calls'
---

Feel free to propose agenda items by commenting on this issue!

# Details

Date: TBD  
Time [UTC](https://time.is/UTC): 19:00  
Time [CET](https://time.is/CET): 20:00  
Time [EST](https://time.is/EST): 14:00  
Link to join: TBD  

# Notes

To be published.

# Agenda

## Keyoxide Project

No items yet.

## Clients & apps

No items yet.

## Libraries

No items yet.

## Identity claims & proofs

No items yet.

## Documentation

No items yet.

## Specifications

No items yet.

## Open discussion & questions